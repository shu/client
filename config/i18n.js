const en = require('../static/i18n/en-US.json')
const zh = require('../static/i18n/zh-TW.json')

module.exports = {
  locales: [
    {
      code: 'zh',
      iso: 'zh-TW',
      name: '中文'
    },
    {
      code: 'en',
      iso: 'en-US',
      name: 'English'
    }
  ],
  defaultLocale: 'en',
  parsePages: false, // Disable babel parsing

  // differentDomains: true,
  // routes: {
  //   login: {
  //     fr: '/login',
  //     en: '/login'
  //   },
  // },
  vueI18n: {
    fallbackLocale: 'en',
    messages: { en, zh }
  }
}
