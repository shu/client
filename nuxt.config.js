require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })
const path = require('path')
const fs = require('fs')

const colors = require('vuetify/es5/util/colors').default
let serverSetting = {}
if (process.env.NODE_ENV === 'development') {
  serverSetting = {
    https: {
      key: fs.readFileSync(path.resolve(__dirname, 'server.key')),
      cert: fs.readFileSync(path.resolve(__dirname, 'server.crt'))
    }
  }
}

module.exports = {
  server: serverSetting,
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/axios',
    '~/plugins/i18n.js',
    '~/plugins/vee-validate',
    '~/plugins/vue-loading-overlay'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify',
    ['@nuxtjs/dotenv', { filename: `.env.${process.env.NODE_ENV}` }]
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/proxy',
    [
      'nuxt-i18n',
      {
        lazy: true,
        loadLanguagesAsync: true,
        locales: [
          {
            name: 'English',
            code: 'en',
            iso: 'en-US',
            file: 'en-US.js'
          },
          {
            code: 'zh',
            iso: 'zh-TW',
            name: '中文',
            file: 'zh-TW.js'
          }
        ],
        langDir: 'static/locales/',
        defaultLocale: 'en',
        fallbackLocale: 'en'
        // strategy: 'prefix',
        //   detectBrowserLanguage: {
        //     useCookie: true,
        //     cookieKey: 'i18n_redirected',
        //   },
        //   rootRedirect: 'en'
      }
    ]
  ],
  // router: {
  //   middleware: ['auth']
  // },
  // env: {
  //   FB_CLIENT_ID: process.env.FB_CLIENT_ID
  // },

  auth: {
    plugins: ['~/plugins/auth.js'],

    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/auth/login',
            method: 'post',
            propertyName: 'token.accessToken'
          },
          logout: { url: '/api/auth/logout', method: 'post' },
          user: { url: '/api/auth/user', method: 'get', propertyName: 'user' }
        }
      }
      // facebook: {
      //   client_id: '228172421849312',
      //   userinfo_endpoint: 'https://graph.facebook.com/v2.12/me?fields=name,email,first_name',
      //   scope: ['public_profile', 'email']
      // },
    }
  },
  axios: {
    proxy: true
  },
  proxy: {
    '/api': process.env.API_URL
  },

  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  build: {
    // only ssr need to transpile or will have error MODULE_NOT_FOUND
    transpile: ['vee-validate/dist/rules', 'vee-validate/dist/locale'],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
