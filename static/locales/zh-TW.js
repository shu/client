import zh from 'vee-validate/dist/locale/zh_TW'

export default {
  validation: Object.assign(zh.messages, {
    verifyPassword: '須十個字元並包含一個大寫字母,一個小寫字母,一個數字'
  }),
  common: {
    email: '信箱',
    password: '密碼'
  },
  layout: {
    login: '登入',
    logout: '登出'
  },
  login: {
    error: {
      INVALID_INPUT: '輸入錯誤'
    }
  },
  member: {
    status: {
      '0': '禁止',
      '1': '正常'
    }
  },

  product: {
    name: '名稱',
    description: '描述',
    price: '價格',
    image: '檔案',
    status: {
      0: '禁止',
      1: '正常'
    }
  }
}
