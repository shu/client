import en from 'vee-validate/dist/locale/en'
export default {
  validation: Object.assign(en.messages, {
    verifyPassword:
      'The password need at least 10 character and must contain 1 uppercase letter, 1 lowercase letter, 1 number'
  }),
  common: {
    email: 'Email',
    password: 'Password'
  },
  layout: {
    login: 'Login',
    logout: 'Logout'
  },
  login: {
    error: {
      INVALID_INPUT: 'Invaild input'
    }
  },
  member: {
    status: {
      0: 'Ban',
      1: 'Normal'
    }
  },

  product: {
    name: 'Name',
    description: 'Description',
    price: 'Price',
    image: 'File',
    status: {
      0: 'Ban',
      1: 'Normal'
    }
  }
}
