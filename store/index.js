// export const actions = {
//   nuxtServerInit({ commit }, { req }) {
//     console.log(req.headers.cookie)
//     // if auth token used by external API, fetch here e.g. req.session.authToken
//     // then it can be accessed with store on all API calls
//     if (req.session && req.session.authUser) {
//       commit('auth/authenticated', true)
//       commit('auth/user', req.session.authUser)
//     } else {
//     }
//   }
// }

export const state = () => ({
  redirect: '/'
})

export const mutations = {
  setRedirect(state, payload) {
    state.redirect = payload
  }
}
// export const actions = {
//   login({ commit }, data) {
//     commit('authenticated', true)
//     commit('user', data.data.user.id)
//   },

//   logout({ commit }) {
//     commit('authenticated', false)
//     commit('user', '')
//   }
// }
// export const getters = {
//   isAuth: (state) => state.authenticated
// }
