import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
// #Rules https://logaretm.github.io/vee-validate/guide/rules.html#importing-rules-in-nuxt-js

import {
  required,
  email,
  max,
  min_value,
  image,
  numeric,
  regex
} from 'vee-validate/dist/rules'

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('extend', extend)
extend('email', email)

extend('required', required)
extend('max', max)
extend('min_value', min_value)
extend('image', image)
extend('numeric', numeric)
extend('regex', regex)

// message need to assign at locales
extend('verifyPassword', {
  validate: (value) => {
    const strongRegex = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{10,})'
    )
    return strongRegex.test(value)
  }
})
