export default async ({ app, $auth }) => {
  $auth.onRedirect((to, from) => {
    if (to.includes(`/${app.i18n.locale}/`)) {
      app.store.commit('setRedirect', to)
      return app.to
    } else {
      app.store.commit('setRedirect', app.localePath(to))
      return app.localePath(to)
    }
  })
}
