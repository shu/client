import Vue from 'vue'
import VueLoading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'

// vue-loading-overlay
Vue.use(VueLoading, {
  canCancel: false,
  color: '#000000',
  loader: 'dots', // spinner/dots/bars
  width: 50,
  height: 50,
  backgroundColor: '#ffffff',
  isFullPage: true,
  opacity: 0.8
}) // Use default options
